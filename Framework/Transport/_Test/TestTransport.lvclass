﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="15008000">
	<Property Name="EndevoGOOP_ClassItemIcon" Type="Str">BlueCircle</Property>
	<Property Name="EndevoGOOP_ClassProvider" Type="Str">Endevo LabVIEW Native</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">1179848</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">12124142</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">9341183</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ConnectorPanePattern" Type="Str">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="EndevoGOOP_PlugIns" Type="Str"></Property>
	<Property Name="EndevoGOOP_TemplateUsed" Type="Str">NativeSubTemplate_4x4x4</Property>
	<Property Name="EndevoGOOP_TemplateVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">Transport.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../Transport.lvlib</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,J!!!*Q(C=T:;`&lt;B."%)@H%II5#/3/DMD+!S".B93IX&amp;`FHGJ[+C-B*6O[1++?6\$]"G[AHS[V&amp;?5&amp;X+2!(!&lt;T?&lt;X_=Y[$GS"FTX-Z`X[TM^`NLF=2S?V-Z*EMDL7,UHCE4Y\N1^N@30YM6I&lt;MF#A0O?NC@4PM\T`&gt;[\]?_I"`K*&amp;]@3U0N5@RN[WJG_:BP[G8%^8+W0&amp;8\F\'RN_[L9S.8\@]_8W`W6;9T_@V!&lt;^ET/=0_G1U]X`[R8Z[@N0SGW9](L@HP[YX0K]ZTKW^@P8+XX(XVT^H8)SX^P\_E,LFXP&gt;J/_Z"P^7/_U=;/?FIUH^O)N8[&amp;RW%%U&lt;I[LA)^%!0^%!0^%"X&gt;%&gt;X&gt;%&gt;X&gt;%=X&gt;%-X&gt;%-X&gt;%.8&gt;%68&gt;%68&gt;&amp;W?*MM,8&gt;!&amp;843@--(AQ5""U;"!E!S+",?!*_!*?!)?PEL!%`!%0!&amp;0Q%/+"$Q"4]!4]!1]&gt;*/!*_!*?!+?A)&gt;3G34SB1Z0Q%.Z=8A=(I@(Y8&amp;Y'&amp;)=(A@!'=QJ\"1"1RT4?8"Y("[(BU&gt;R?"Q?B]@B=8CQR?&amp;R?"Q?B]@BI5O?&amp;=]USQM&gt;(MK)Q70Q'$Q'D]&amp;$;4&amp;Y$"[$R_!R?"B/$"[$RY!Q"D3+AS"'*S0"_',Q'$T]%90(Y$&amp;Y$"[$"SOPE/7:7&gt;)M,X2Y&amp;"[&amp;2_&amp;2?"1?3ID#I`!I0!K0QE.:58A5(I6(Y6&amp;Y'%I5(I6(Y6&amp;!F%%:8J2C3E=F32%5(DZZNWB?*=]EGK`SUVRN6#E&lt;5-L'EL*BJ'Q%+1MM:?'E,)C5C:9SA6)G2MI,3XE2+9"3"J:35%KCT,B0C1ER)I&lt;%A/A40;*,&gt;*:&gt;(TFR.JP*&gt;$K6S71CI^&amp;)BM/B$!9$[@@\UOPVJ.PN3K@4W:R7ZVSL6KX0J@4RV&gt;@U?8"T^_MN=5FU&lt;O\1PZ#TDH4SL5JH&lt;[JU`LR+[&lt;2+1HRY5;60\[NU_;&gt;+6Q&gt;CM&lt;B.LX``4#^`X+&lt;4G_^*LODXDJ!=KX0J+:S.=L,[FWE\2X]"J&amp;:7@A!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.CoreWirePen" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!6*0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0DEX.$!Z-D1],V:B&lt;$Y.#DQP64-S0AU+0&amp;5T-DY.#DR/97VF0E*B9WNH=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0DAW/41W.D)],V:B&lt;$Y.#DQP64-S0AU+0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z';7RM)&amp;"B&gt;(2F=GY],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$!],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!R0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-DQP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$-],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!U0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.4QP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$9],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!X0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I],U.M&gt;8.U:8)_$1I]34%W0AU+0%ZB&lt;75_6WFE&gt;'A],UZB&lt;75_$1I]6G&amp;M0D%],V:B&lt;$Y.#DQP34%W0AU+0%680AU+0%ZB&lt;75_47^E:4QP4G&amp;N:4Y.#DR$;'^J9W5_1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z&amp;?'.M&gt;8.J&gt;G5A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"$&lt;X"Z0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X)A28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"#;81A1WRF98)],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;6TY.#DR&amp;4$Y.#DR/97VF0F.U?7RF0#^/97VF0AU+0%.I&lt;WFD:4Z4&lt;WRJ:$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I0#^$;'^J9W5_$1I]1WBP;7.F0E2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;#"%&lt;X1],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E:J&lt;'QA5H6M:4QP4G&amp;N:4Y.#DR$;'^J9W5_28:F&lt;C"0:'1],U.I&lt;WFD:4Y.#DR$;'^J9W5_6WFO:'FO:TQP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0%6-0AU+0%ZB&lt;75_27ZE)%.B=(-],UZB&lt;75_$1I]1WBP;7.F0E2F:G&amp;V&lt;(1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2GRB&gt;$QP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0#^$&lt;(6T&gt;'6S0AU+!!!!!!</Property>
	<Property Name="NI.LVClass.EdgeWirePen" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!5Y0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D%T-4%Y.TQP6G&amp;M0AU+0#^6-T)_$1I]64-S0AU+0%ZB&lt;75_1G&amp;D;W&gt;S&lt;X6O:#"$&lt;WRP=DQP4G&amp;N:4Y.#DR797Q_.49W.T1R-4QP6G&amp;M0AU+0#^6-T)_$1I]1WRV=X2F=DY.#DR/97VF0E:J&lt;'QA5'&amp;U&gt;'6S&lt;DQP4G&amp;N:4Y.#DR/&gt;7V&amp;&lt;(2T0DA],UZV&lt;56M&gt;(-_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-$QP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!R0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$)],UZB&lt;75_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-TQP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!U0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$5],UZB&lt;75_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.DQP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!X0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0#^$&lt;(6T&gt;'6S0AU+0%ER.DY.#DR/97VF0F&gt;J:(2I0#^/97VF0AU+0&amp;:B&lt;$YT0#^797Q_$1I],UER.DY.#DR&amp;6TY.#DR/97VF0EVP:'5],UZB&lt;75_$1I]1WBP;7.F0E.P=(E],U.I&lt;WFD:4Y.#DR$;'^J9W5_4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0E*J&gt;#"$&lt;'6B=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_4G^S)%6Y9WRV=WFW:3"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP26=_$1I]25Q_$1I]4G&amp;N:4Z4&gt;(FM:4QP4G&amp;N:4Y.#DR$;'^J9W5_5W^M;71],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1A2'^U0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP25Q_$1I]25Q_$1I]4G&amp;N:4Z';7RM)&amp;*V&lt;'5],UZB&lt;75_$1I]1WBP;7.F0E6W:7YA4W2E0#^$;'^J9W5_$1I]1WBP;7.F0F&gt;J&lt;G2J&lt;G=],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E6O:#"$98"T0#^/97VF0AU+0%.I&lt;WFD:4Z%:7:B&gt;7RU0#^$;'^J9W5_$1I]1WBP;7.F0E:M981],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DQP1WRV=X2F=DY.#A!!!!!</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"&lt;O5F.31QU+!!.-6E.$4%*76Q!!%J1!!!2;!!!!)!!!%H1!!!!K!!!!!A^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!!!!!#1&amp;1#!!!!Q!!!)!!!!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!&amp;;!S&lt;SMU^J#OY_W&amp;ZG07V]!!!!-!!!!%!!!!!#W:.M-:/\(1\4SX[+O&lt;V+8V"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%!!!$`````!!!!%!!!!!!!!!!!!!!!!!!!!!!!!!!%!!!!!!!!!#=!!5R71U-!!!!"!!*735R#!!!!!&amp;"53$!!!!!&amp;!!%!!1!!!!!#!!-!!!!!!A!"!!!!!!!G!!!!*HC=9_"E9'JAO-!!R)R!T.4!^!0)`A$C-QBQ1'59!-@U#P%!!!!!!%=!!!%9?*RD9-!%`Y%!3$%S-$#&gt;!N)M;/*A'M;G*M"F,C[\I/,-5$?S1E1:A7*-?Y!-*J!=QC]AQ(13C%_AG]/)R7Q!?.UI&amp;!!!!!!-!!&amp;73524!!!!!!!$!!!"C1!!!LRYH'NA:'$).,9QOQ#EG9&amp;9F+'")4E`*:7,!=BHA)!F4!QEAQ#I@CUU==-$B^/!1)^@PA8-&lt;X\$U_WC)N"=IS,"6#L3\;-CUOGDQN,*IP,CT````ZO0]"TO^MAZ\GA$5NP.!21`\K,#!?)!;291`4]Q![1+:FY!U$3/BAJFBB)7QQ.2BY]XG$"#,)9:'96K0^"'E!.9/DP%A-\N$F%2[*QA"71V7$(Q4TX19!EE$X;=-DD%P_V3]S&gt;'AY-\1-&lt;RORY#7NA!=4](U!C"AQ^:OBMVA0T?C2J15TN$/#3/OX$IC!(ZD#=[18&lt;QQ(T&amp;!8&gt;P'-C!%K#N*C#@M9"-"[PJ:DPOI!(WJY-)B-K!5"51KA"%\1#\Y!B(X'&amp;Y_+Z^@7]8+.S2I]Q!C%(K^2A9'2D"=IQ-N6!Z'S#&lt;#3I'CSM1GQ%;&lt;"J)?F19%?;"^)"E\E$VA.C0I'S187R1-6WAHAF1.CAK%K"M;S$\!J4N"'1,1.G?)$9DB/U(:4P\O\ACJS&amp;9?A1!C6&gt;[N!!!!!!!!!Y6!9!D!!!'-45O-#YR!!!!!!!!$"5!A!!!!!1R.3YQ!!!!!!Y6!9!D!!!'-45O-#YR!!!!!!!!$"5!A!!!!!1R.3YQ!!!!!!Y6!9!D!!!'-45O-#YR!!!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y"R')_!?X@@]`MZX`0\@N`T_R(@]````YQ!!!'-!!!"D!1!!9!,Q!'!%BA"A#)$A9"#!9'!AA/"A9)'A9(S"I'"`AS"A@`9A9(`])'"``#"A@`]A9(`^Y'"``%"A@`S!9(`^A'!`_1"A"`I!9!"]!'!!#!"A!!!!@````]!!!)!``````````````````````!!!!!0`Q!0!!`Q!0!!```Q!!!!$``Q`Q``$```$`````]!````]0]!``!0`Q``````!0````$`$```]0]0`````Q$````Q`Q!0!!``$`````]!``````````````````!!)A!!!!!!!!!!!!!!!!`Q!#)!!!!!!!!!!!!!!!!0]!!C!!!!QMQ!!!!!!!!!$`!!!!!!$/:G9MQ!!!!!!!`Q!!!!!-*G:G:G,-!!!!!0]!!!!!QG:G:G:G:OU!!!$`!!!!!#:G:G:G:G:C!!!!`Q!!!!ZG:G:G:G:GZA!!!0]!!!$G:G:G:G:G&lt;G9!!!$`!!!!\OZG:G:G:O:G!!!!`Q!!!/:G&lt;O:G:GZG:A!!!0]!!!!G:G:G\G&lt;G:G9!!!$`!!!!*G:G:G:C:G:G!!!!`Q!!!#:G:G:G&lt;G:G9A!!!0]!!!!G:G:G:GZG:G)!!!$`!!!!*G:G:G:O:G:C!!!!`Q!!!#:G:G:G&lt;G:G&lt;!!!!0]!!!!G:G:G:GZG:M!!!!$`!!!!:G:G:G:O:G)!!!!!`Q!!!-XG:G:G&lt;G9A!!!!!0]!!!!!$.ZG:GZCQ!!!!!$`!!!!!!!!T?:O&lt;!!!!!!!`Q!!!!!!!!!-UM!!!!!!!0]!!!!!!!!!!!!!!!!!!!$`````````````````````]!!!1!````````````````````````````````````````````*#1E*#1E*#4```]!!!$`!!!!``]!!!$`!!!!``````]E*#1E*#1E*0````]!``]!````!0``````!0```````````S1E`````````Q$``Q!!````!!$```]!````````````*#4`````````!0``!0```````Q$``Q$```````````]E*0````````]!``]!!!$`!!!!````!0```````````S1E````````````````````````````````````!!!!U^-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!$4UQ!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!.04!!!!!!!!^K.Z_!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!0&lt;(?++=RJV6^A!!!!!!!!!!!!!!``]!!!!!!!!!!!$WIXBYIHBY?(D'IXEL!!!!!!!!!!$``Q!!!!!!!!!!^K.Y?(CC?(BY?(BY?*T(A!!!!!!!!0``!!!!!!!!!!#D?(BY?+*Y?(BY?(BY?*T0!!!!!!!!``]!!!!!!!!!J(BY?(BYIHBY?(BY?(C=R]9!!!!!!!$``Q!!!!!!!+7=?(BY?(CC?(BY?(BYH-&gt;YRA!!!!!!!0``!!!!!!!!R]@(RZRY?+*Y?(BY?(D(H(D'!!!!!!!!``]!!!!!!!$(H*S=RM@(RHBY?(BYRZRY?-9!!!!!!!$``Q!!!!!!!+/=H*S=H*T'R]@'?-?=?(BYRA!!!!!!!0``!!!!!!!!IZS=H*S=IJS=H-&lt;/H(BY?(D'!!!!!!!!``]!!!!!!!#DH*S=H++=H*S=H-?=?(BY?+-!!!!!!!$``Q!!!!!!!+/=H*SCH*S=H*S=R[+CIHBYIQ!!!!!!!0``!!!!!!!!IZS=IJS=H*S=H*T(H(CCIK+D!!!!!!!!``]!!!!!!!#DH++=H*S=H*S=H-?=?(BYH(E!!!!!!!$``Q!!!!!!!+/CH*S=H*S=H*S=RZRY?*RZ!!!!!!!!!0``!!!!!!!!IJS=H*S=H*S=H*T(H(C=IQ!!!!!!!!!!``]!!!!!!!$YA-?CH*S=H*S=H-&gt;Y?+-!!!!!!!!!!!$``Q!!!!!!!!!!!0C!R];=H*S=RXCD^A!!!!!!!!!!!0``!!!!!!!!!!!!!!!!_(L(RJT(RCM!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!L?KML!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0```````````````````````````````````````````Q!!!!)!!1!!!!!!$!!"2F")5!!!!!!!!Q!!!X=!!!D&amp;?*SN6EN05V%1HF.;=YI94J%#.7!@HF93)6%552\;Q%8$_S5'8+C.2=%1;SA96L#Z)='%&amp;1E,%\:M8&lt;"QQU*.Y[9&lt;6SZEU@!0."%VSOVVTLVN\_UDL1;\G$4.@00.T0?&gt;31(I/V:H3=++!I1&gt;YJ&gt;""=L$=1)1;[;1_A47A%W3XU#K853"%4L*^CV*5K^!24A?I"?F&gt;@C3SC;NFCZWA+EHG!O,F3N1'9\8/PJZAP(8^8T&gt;FK\KA!;W1:+7+?\_14@E#"+#X#CCIZEEA5B?KV8WD9;?T=B=`'JPJC[NJ&amp;U"*M5&lt;&amp;HDC0&amp;:%[P&gt;;35M4Y5C&gt;+AF9MB&amp;W&gt;X=.E%-("&lt;1WLC''=+X&gt;JC+9+M2%?;**QZ2L'/2J4@.)\O3G[&amp;W!=K'HJ4B$+/+7U[PB;3\*L11,YG96K/;*&lt;OKC"_(0NF.D]BMA1')0K0J.`3\QDG'BAJ:&lt;B5,YWQG&lt;Q/]4#FS3YZ;(9%5O.9FEX&lt;#JS7".SX":S(",E]%OL4-Q&gt;.AMI9.ED@E;?O;8IIMT#Z\)9]_D_6!U[HG_-0=CN$DD#9=71\J#T+`XBCDAD13(]*N&amp;OS,&amp;87VC)9*@^YM4L"#%I&amp;G!#/TM\/"/-"L16I47]E1'ZU!W#;3-#'&amp;DG;)29ZF8=:H_;@$@!_HHH3&gt;CL@ZW3\;:*=0-V^(-$J&lt;&lt;8*;:/`[0G5OMKB.NNZ,D&lt;_C%#T"5R+N&gt;/MDQ._9,6'=2T!X%L*L^D2B%J(G+_@NGHL]&amp;.M@@7VN&lt;O@Y/:C4287YF2(?Z`&amp;%^5I_%VY`58^!"`3;PD["/S#&gt;-KXZ&amp;JHJESN*F3/BS7^0&amp;;&lt;D&lt;BM,Y3\J&lt;^AV(]KZ-B1,$[,I7HLCLF;T53N&lt;!/@"##UZW&gt;A$LC/BQ[[_-*-M]-&lt;?_C*.1+8OU&amp;+_)T*&gt;;ZF0K%_8T5]/K3-69/P8Y6?VO9]:2H,''*ZT'S\*"!.JQQ,W^02Q19[%"H&lt;A$DZ&lt;C&amp;4&amp;4@S[`&amp;4V6'-%L9OH5YV=V$TCG8ZVJEYBC2L3KW&gt;;&amp;:DS$;`"I+6Y2-R23@D=&amp;5]7$_\P-@S^KHH"=HX$+.''&amp;`BDT*C4:$'Z-+&gt;4-&lt;"9&amp;PNE"URE&gt;N_NHN!T0[.J^)14G$JNO$.X`M*4ZYU!HO+173W#^3,'KA&amp;0KK8L,_E34N)`WMGU]S8C&gt;;_AW09R^SPSVC!VGE0*(?%G7_3MXL@M$)OF1=A!!!!!%!!!!+1!!!!1!!!!!!!!!$!!"1E2)5!!!!!!!!Q!!!'5!!!"V?*RD9'!I&amp;*"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```V=J9O4Y?O1;8.%2(TB4::9]BQ1!:1A:GA!!!!!!!!1!!!!(!!!#4Q!!!!=!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S!!!!4"5!A!!!!!!"!!A!-0````]!!1!!!!!!-!!!!!)!#E!B"%ZP&lt;G5!!"Z!5!!"!!!66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!%!!1!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)!!!!J&amp;1#!!!!!!!)!"1!(!!!-!%!!!@````]!!!!"!!%!!!!"!!!!!!!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ!!!!'25!A!!!!!!"!!5!"Q!!!1!!R^"C61!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!!!!!:&amp;1#!!!!!!!%!"1!(!!!"!!$(U'*6!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9Q!!!%Q6!)!!!!!!!1!)!$$`````!!%!!!!!!$!!!!!#!!J!)12/&lt;WZF!!!?1&amp;!!!1!!&amp;62F=X25=G&amp;O=X"P=H1O&lt;(:D&lt;'&amp;T=Q!"!!%!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:1!!!"E6!)!!!!!!!1!&amp;!!-!!!%!!!!!!!%!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B!!!!/25!A!!!!!!#!!J!)12/&lt;WZF!!!?1&amp;!!!1!!&amp;62F=X25=G&amp;O=X"P=H1O&lt;(:D&lt;'&amp;T=Q!"!!%!!!!!!!!!!!!%!!-!#Q!!!!1!!!"3!!!!+!!!!!)!!!1!!!!!+1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$[!!!"E8C=D9`.4M*!&amp;)7`&gt;CK5#A)+\ES'D1M8&lt;HS"*C9O$4'YN`4(9#9-[1T%J7`HVE@2.`"3-'L=G*/Z0_??H(M(/#6**^'N8:&lt;QR-@LW`Q?50V:H3X&gt;SN&lt;_UGT-9D[?F=\`Z(+4/4@]T?&lt;?=*&amp;/#?$MWKS&gt;,WNN+^V)^;J?&lt;$*@[C,T'1=#%AI)XAGF5@O7+S)[;2N6G5@C^%8:/\]D3FMR%(&gt;&amp;CT;R7B?6;#&gt;UGYW*]M_&amp;?%T&amp;LM-BZ`_])U2#;RN%(@-A*U4=C)XGC,[-NRA)^Y6QD\`-^W4990?BA'/J]M9XE!=^S5I7&gt;K5[934-C,(50@A%L:"#'A!!!!!!&gt;Q!"!!)!!Q!&amp;!!!!7!!0"!!!!!!0!.A!V1!!!'%!$Q1!!!!!$Q$9!.5!!!"K!!]%!!!!!!]!W!$6!!!!=Y!!B!#!!!!0!.A!V1!!!(7!!)1!A!!!$Q$9!.5)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65E"-A%Q!&amp;*45E-.#A!$4&amp;:$1UR#6F=!!"+5!!!%7A!!!#!!!"*U!!!!!!!!!!!!!!!A!!!!.!!!"%A!!!!&gt;4%F#4A!!!!!!!!&amp;M4&amp;:45A!!!!!!!!'!5F242Q!!!!!!!!'51U.46!!!!!!!!!'I4%FW;1!!!!!!!!']1U^/5!!!!!!!!!(16%UY-!!!!!!!!!(E2%:%5Q!!!!!!!!(Y4%FE=Q!!!!!!!!)-6EF$2!!!!!!!!!)A&gt;G6S=Q!!!!1!!!)U5U.45A!!!!!!!!+92U.15A!!!!!!!!+M35.04A!!!!!!!!,!;7.M.!!!!!!!!!,5;7.M/!!!!!!!!!,I1V"$-A!!!!!!!!,]4%FG=!!!!!!!!!-12F")9A!!!!!!!!-E2F"421!!!!!!!!-Y6F"%5!!!!!!!!!.-4%FC:!!!!!!!!!.A1E2)9A!!!!!!!!.U1E2421!!!!!!!!/)6EF55Q!!!!!!!!/=2&amp;2)5!!!!!!!!!/Q466*2!!!!!!!!!0%3%F46!!!!!!!!!096E.55!!!!!!!!!0M2F2"1A!!!!!!!!1!!!!!!0````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Q!!!!!!!!!!$`````!!!!!!!!!-1!!!!!!!!!!0````]!!!!!!!!!W!!!!!!!!!!!`````Q!!!!!!!!$A!!!!!!!!!!$`````!!!!!!!!!1Q!!!!!!!!!!0````]!!!!!!!!"&amp;!!!!!!!!!!!`````Q!!!!!!!!&amp;!!!!!!!!!!!$`````!!!!!!!!!9Q!!!!!!!!!!0````]!!!!!!!!"H!!!!!!!!!!%`````Q!!!!!!!!-M!!!!!!!!!!@`````!!!!!!!!!U!!!!!!!!!!#0````]!!!!!!!!$5!!!!!!!!!!*`````Q!!!!!!!!.E!!!!!!!!!!L`````!!!!!!!!!X1!!!!!!!!!!0````]!!!!!!!!$C!!!!!!!!!!!`````Q!!!!!!!!/A!!!!!!!!!!$`````!!!!!!!!!\1!!!!!!!!!!0````]!!!!!!!!%/!!!!!!!!!!!`````Q!!!!!!!!9]!!!!!!!!!!$`````!!!!!!!!#E!!!!!!!!!!!0````]!!!!!!!!+3!!!!!!!!!!!`````Q!!!!!!!!J9!!!!!!!!!!$`````!!!!!!!!$&gt;1!!!!!!!!!!0````]!!!!!!!!.X!!!!!!!!!!!`````Q!!!!!!!!XE!!!!!!!!!!$`````!!!!!!!!$@1!!!!!!!!!!0````]!!!!!!!!/9!!!!!!!!!!!`````Q!!!!!!!!ZI!!!!!!!!!!$`````!!!!!!!!%,Q!!!!!!!!!!0````]!!!!!!!!1R!!!!!!!!!!!`````Q!!!!!!!"$-!!!!!!!!!!$`````!!!!!!!!%0A!!!!!!!!!A0````]!!!!!!!!2_!!!!!!26'6T&gt;&amp;2S97ZT='^S&gt;#ZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>
<Name></Name>
<Val>!!!!!A^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!!!!!!!"!!%!!!!!!!!!!!!!!A!+1#%%4G^O:1!!#!"1!!%!!!!"!!%!!!!"`````A!!!!!"%&amp;2F=X2$98.F,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!"5!A!!!!!!!!!!!!!!$!!!!'&amp;2F&lt;8"M982F6'6T&gt;%.B=W5O&lt;(:D&lt;'&amp;T=Q!!!":5:8.U36:J:8&gt;.&lt;W2F&lt;#ZM&gt;G.M98.T!!!!&amp;62F=X25=G&amp;O=X"P=H1O&lt;(:D&lt;'&amp;T=Q</Val>
</String>
</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"H!!!!!2"5:8.U1W&amp;T:3ZM&gt;G.M98.T!&amp;"53$!!!!"*!!!!"A=]&gt;GFM;7)_"G&amp;E:'^O=QV@3EN*)&amp;2P&lt;WRL;82T#6:*)&amp;2F=X2F=AR5:8.U1W&amp;T:3ZM&lt;')16'6T&gt;%.B=W5O&lt;(:D&lt;'&amp;T=Q!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="TestTransport.ctl" Type="Class Private Data" URL="TestTransport.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
	<Item Name="setUp.vi" Type="VI" URL="../setUp.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;'!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$8*F:G6S:7ZD:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!_1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!RS:7:F=G6O9W5A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="tearDown.vi" Type="VI" URL="../tearDown.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;'!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$8*F:G6S:7ZD:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!_1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!RS:7:F=G6O9W5A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!A!!?!!!#1!!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
	</Item>
	<Item Name="Test_AbstractTransport_ReturnError.vi" Type="VI" URL="../Test_AbstractTransport_ReturnError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="Test_BooleanTextBinding_ValueOK.vi" Type="VI" URL="../Test_BooleanTextBinding_ValueOK.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">17039616</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="Test_ConfigurationFileBinding_ValueOK.vi" Type="VI" URL="../Test_ConfigurationFileBinding_ValueOK.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1073741856</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_ControlCaptionBinding_ValueOK.vi" Type="VI" URL="../Test_ControlCaptionBinding_ValueOK.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1074004256</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_EventTransport_ValueOK.vi" Type="VI" URL="../Test_EventTransport_ValueOK.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="Test_FormattedStringBinding_ValueOK.vi" Type="VI" URL="../Test_FormattedStringBinding_ValueOK.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">17039616</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="Test_ListboxItemNamesBinding_ValueOK.vi" Type="VI" URL="../Test_ListboxItemNamesBinding_ValueOK.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1074004256</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_MCListboxItemNamesBinding_ValueOK.vi" Type="VI" URL="../Test_MCListboxItemNamesBinding_ValueOK.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">17039616</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="Test_NamedNumericStringsBinding_ValueOK.vi" Type="VI" URL="../Test_NamedNumericStringsBinding_ValueOK.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1074004256</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_PersistToDisk_OneWrite_ValueOK.vi" Type="VI" URL="../Test_PersistToDisk_OneWrite_ValueOK.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107825168</Property>
	</Item>
	<Item Name="Test_PersistToDisk_Overwrite_ReturnLastValue.vi" Type="VI" URL="../Test_PersistToDisk_Overwrite_ReturnLastValue.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107825170</Property>
	</Item>
	<Item Name="Test_PersistToDisk_OverwriteVarLength_ReturnLastValue.vi" Type="VI" URL="../Test_PersistToDisk_OverwriteVarLength_ReturnLastValue.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_QueueTransport_ValueOK.vi" Type="VI" URL="../Test_QueueTransport_ValueOK.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="Test_StreamToDisk_ValueOK.vi" Type="VI" URL="../Test_StreamToDisk_ValueOK.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="Test_TerminalBinding_ValueOK.vi" Type="VI" URL="../Test_TerminalBinding_ValueOK.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">17039616</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="Test_TerminalEnabledState_BooleanFalse_Disabled.vi" Type="VI" URL="../Test_TerminalEnabledState_BooleanFalse_Disabled.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1074004256</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_TerminalEnabledState_BooleanTrue_Enabled.vi" Type="VI" URL="../Test_TerminalEnabledState_BooleanTrue_Enabled.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1074004256</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_TerminalEnabledState_EqualToZero_Disabled.vi" Type="VI" URL="../Test_TerminalEnabledState_EqualToZero_Disabled.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1090781440</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
	</Item>
	<Item Name="Test_TerminalEnabledState_GreaterThanZero_Enabled.vi" Type="VI" URL="../Test_TerminalEnabledState_GreaterThanZero_Enabled.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1074004256</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_TerminalReference_ValueOK.vi" Type="VI" URL="../Test_TerminalReference_ValueOK.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">17039616</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="Test_TerminalReference_LatchingBoolean_Error.vi" Type="VI" URL="../Test_TerminalReference_LatchingBoolean_Error.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1090781440</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821074</Property>
	</Item>
	<Item Name="Test_TerminalReference_FloatToInteger_ValueOK.vi" Type="VI" URL="../Test_TerminalReference_FloatToInteger_ValueOK.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">17039616</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="Test_TerminalReference_IntegerToFloat_ValueOK.vi" Type="VI" URL="../Test_TerminalReference_IntegerToFloat_ValueOK.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">17039616</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
	</Item>
	<Item Name="Test_TerminalVisibility_BooleanFalse_NotVisible.vi" Type="VI" URL="../Test_TerminalVisibility_BooleanFalse_NotVisible.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1074004256</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_TerminalVisibility_BooleanTrue_Visible.vi" Type="VI" URL="../Test_TerminalVisibility_BooleanTrue_Visible.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1074004256</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_TerminalVisibility_EqualToZero_NotVisible.vi" Type="VI" URL="../Test_TerminalVisibility_EqualToZero_NotVisible.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1074004256</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_TerminalVisibility_GreaterThanZero_Visible.vi" Type="VI" URL="../Test_TerminalVisibility_GreaterThanZero_Visible.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1074004256</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_ValueReferenceTransport_ValueOK.vi" Type="VI" URL="../Test_ValueReferenceTransport_ValueOK.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1073741856</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
	<Item Name="Test_ValueReferenceTransport_NoWrite_ReturnError.vi" Type="VI" URL="../Test_ValueReferenceTransport_NoWrite_ReturnError.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H$V2S97ZT='^S&gt;#ZM&gt;GRJ9B65:8.U6(*B&lt;H.Q&lt;X*U,GRW9WRB=X-!$&amp;2F=X2$98.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!]1(!!(A!!*Q^5=G&amp;O=X"P=H1O&lt;(:M;7)66'6T&gt;&amp;2S97ZT='^S&gt;#ZM&gt;G.M98.T!!N5:8.U1W&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1073741856</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
	</Item>
</LVClass>
